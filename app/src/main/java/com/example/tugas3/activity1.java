package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class activity1 extends AppCompatActivity implements View.OnClickListener {

    EditText editTextNumber;
    Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9;
    Button tambah, kali, kurang, bagi;
    Button hapus, hasil;

    public static double nilaiSekarang=0;
    public static String operasiSekarang="";
    public static double hasilAkhir=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        init();
    }

    public void init(){
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        bt0 = (Button) findViewById(R.id.bt0);
        bt0.setOnClickListener(this);
        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button) findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button) findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button) findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button) findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button) findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button) findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button) findViewById(R.id.bt9);
        bt9.setOnClickListener(this);

        tambah = (Button) findViewById(R.id.tambah);
        tambah.setOnClickListener(this);
        kali = (Button) findViewById(R.id.kali);
        kali.setOnClickListener(this);
        kurang = (Button) findViewById(R.id.kurang);
        kurang.setOnClickListener(this);
        bagi = (Button) findViewById(R.id.bagi);
        bagi.setOnClickListener(this);

        hapus = (Button) findViewById(R.id.hapus);
        hapus.setOnClickListener(this);
        hasil = (Button) findViewById(R.id.hasil);
        hasil.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt0:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"0");
                break;
            case R.id.bt1:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"1");
                break;
            case R.id.bt2:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"2");
                break;
            case R.id.bt3:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"3");
                break;
            case R.id.bt4:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"4");
                break;
            case R.id.bt5:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"5");
                break;
            case R.id.bt6:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"6");
                break;
            case R.id.bt7:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"7");
                break;
            case R.id.bt8:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"8");
                break;
            case R.id.bt9:
                editTextNumber.setText(editTextNumber.getText().toString().trim()+"9");
                break;

            case R.id.tambah:
                nilaiSekarang = Double.parseDouble(editTextNumber.getText().toString());
                operasiSekarang="tambah";
                editTextNumber.setText("");
                break;
            case R.id.kali:
                nilaiSekarang = Double.parseDouble(editTextNumber.getText().toString());
                operasiSekarang="kali";
                editTextNumber.setText("");
                break;
            case R.id.kurang:
                operasiSekarang="kurang";
                nilaiSekarang = Double.parseDouble(editTextNumber.getText().toString());
                editTextNumber.setText("");
                break;
            case R.id.bagi:
                operasiSekarang="bagi";
                nilaiSekarang = Double.parseDouble(editTextNumber.getText().toString());
                editTextNumber.setText("");
                break;

            case R.id.hapus:
                editTextNumber.setText("");
                break;
            case R.id.hasil:
                if(operasiSekarang.equals("tambah")){
                    hasilAkhir = nilaiSekarang + Double.parseDouble(editTextNumber.getText().toString().trim());
                    editTextNumber.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("kali")){
                    hasilAkhir = nilaiSekarang * Double.parseDouble(editTextNumber.getText().toString().trim());
                    editTextNumber.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("kurang")){
                    hasilAkhir = nilaiSekarang - Double.parseDouble(editTextNumber.getText().toString().trim());
                    editTextNumber.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("bagi")){
                    hasilAkhir = nilaiSekarang / Double.parseDouble(editTextNumber.getText().toString().trim());
                    editTextNumber.setText(String.valueOf((int)hasilAkhir));
                }

                int tmp = (int) hasilAkhir;
                if(tmp == hasilAkhir){
                    editTextNumber.setText(String.valueOf((int)hasilAkhir));
                }else{
                    editTextNumber.setText(String.valueOf(hasilAkhir));
                }

                break;
        }
    }
}